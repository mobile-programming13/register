import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:register/home/in_app_page.dart';
import 'package:register/session.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _usernameEditingController =
      TextEditingController();
  final TextEditingController _passwordEditingController =
      TextEditingController();

  Future<void> login() async {
    String username = _usernameEditingController.text;
    String password = _passwordEditingController.text;
    if (username == "63160015" && password == "1234") {
      _passwordEditingController.clear();
      await Session.setLogin(true);
      Get.to(() => const InAppPage());
    } else {
      Get.defaultDialog(
        title: "alert".tr.capitalize ?? "alert".tr,
        middleText: 'User username(63160015) or password(1234) is not correct.',
        middleTextStyle: const TextStyle(fontSize: 13),
        textConfirm: "ok".tr.capitalizeFirst,
        onConfirm: () {
          Get.close(0);
        },
      );
      // showDialog(
      //   context: context,
      //   barrierDismissible: false, // user must tap button!
      //   builder: (BuildContext context) {
      //     return AlertDialog(
      //       title: const Text('alert'),
      //       content: SingleChildScrollView(
      //         child: ListBody(
      //           children: [
      //             Center(
      //               child: Column(
      //                 crossAxisAlignment: CrossAxisAlignment.center,
      //                 children: const [
      //                   Text('User username or password is not correct.'),
      //                   Text('Please fill your username and password again'),
      //                 ],
      //               ),
      //             )
      //           ],
      //         ),
      //       ),
      //       actions: [
      //         TextButton(
      //           child: const Text('OK'),
      //           onPressed: () {
      //             Navigator.of(context).pop();
      //           },
      //         ),
      //       ],
      //     );
      //   },
      // );
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: 1,
          title: const Text("BUREG"),
          foregroundColor: const Color.fromARGB(255, 221, 167, 7),
          backgroundColor: Colors.white,
          actions: [
            IconButton(
              onPressed: () {
                Get.updateLocale(
                  Get.locale == const Locale("en")
                      ? const Locale("th")
                      : const Locale("en"),
                );
              },
              icon: const Icon(Icons.language_outlined),
            )
          ],
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Center(
              child: Image.asset(
                "assets/images/logo_buu-03-transformed.png",
                height: 130,
              ),
            ),
            SizedBox(
              width: 250,
              height: 200,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text("username".tr.capitalizeFirst ?? "username".tr),
                  SizedBox(
                    height: 40,
                    child: TextField(
                      autofocus: false,
                      controller: _usernameEditingController,
                      decoration: const InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  Text("password".tr.capitalizeFirst ?? "password".tr),
                  SizedBox(
                    height: 40,
                    child: TextField(
                      obscureText: true,
                      controller: _passwordEditingController,
                      decoration: const InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            TextButton(
              onPressed: () => {login()},
              style: TextButton.styleFrom(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                backgroundColor: const Color.fromARGB(255, 221, 167, 7),
              ),
              child: Text(
                "log in".tr.capitalizeFirst ?? "log in".tr,
                style: const TextStyle(
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
