import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Result extends StatefulWidget {
  const Result({super.key});

  @override
  State<Result> createState() => _ResultState();
}

class _ResultState extends State<Result> {
  var hover63 = false.obs;
  var hover64 = false.obs;
  var hover65 = false.obs;
  var tap63 = true.obs;
  var tap64 = false.obs;
  var tap65 = false.obs;
  final PageController pageController = PageController(
    initialPage: 0,
  );
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      children: [
        summaryBox(),
        const SizedBox(
          height: 20,
        ),
        const Divider(
          thickness: 0.8,
          indent: 100,
          endIndent: 100,
        ),
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 100),
          child: yearChoose(),
        ),
        const SizedBox(
          height: 40,
        ),
        SizedBox(
          height: 500,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: PageView(
              controller: pageController,
              scrollDirection: Axis.horizontal,
              children: [
                Column(
                  children: [
                    Card(
                      child: ExpansionTile(
                        title: Text('first semester'.tr.capitalizeFirst ??
                            'first semester'.tr),
                        controlAffinity: ListTileControlAffinity.trailing,
                        children: const [
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'A',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text('88624559	Software Testing'),
                            subtitle: Text('Dr.Pichet Wayalun'),
                          ),
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'B+',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text(
                                '88624459 Object-Oriented Analysis and Design'),
                            subtitle: Text('Mr.Worawit Werapan'),
                          ),
                        ],
                      ),
                    ),
                    Card(
                      child: ExpansionTile(
                        title: Text('second semester'.tr.capitalizeFirst ??
                            'second semester'.tr),
                        controlAffinity: ListTileControlAffinity.trailing,
                        children: const [
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'A',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text('88624559	Software Testing'),
                            subtitle: Text('Dr.Pichet Wayalun'),
                          ),
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'B+',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text(
                                '88624459 Object-Oriented Analysis and Design'),
                            subtitle: Text('Mr.Worawit Werapan'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Card(
                      child: ExpansionTile(
                        title: Text('first semester'.tr.capitalizeFirst ??
                            'first semester'.tr),
                        controlAffinity: ListTileControlAffinity.trailing,
                        children: const [
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'C+',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text('88624559	Software Testing'),
                            subtitle: Text('Dr.Pichet Wayalun'),
                          ),
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'D+',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text(
                                '88624459 Object-Oriented Analysis and Design'),
                            subtitle: Text('Mr.Worawit Werapan'),
                          ),
                        ],
                      ),
                    ),
                    Card(
                      child: ExpansionTile(
                        title: Text('second semester'.tr.capitalizeFirst ??
                            'second semester'.tr),
                        controlAffinity: ListTileControlAffinity.trailing,
                        children: const [
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'C',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text('88624559	Software Testing'),
                            subtitle: Text('Dr.Pichet Wayalun'),
                          ),
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'B',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text(
                                '88624459 Object-Oriented Analysis and Design'),
                            subtitle: Text('Mr.Worawit Werapan'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Card(
                      child: ExpansionTile(
                        title: Text('first semester'.tr.capitalizeFirst ??
                            'first semester'.tr),
                        controlAffinity: ListTileControlAffinity.trailing,
                        children: const [
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'C+',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text('88624559	Software Testing'),
                            subtitle: Text('Dr.Pichet Wayalun'),
                          ),
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'A',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text(
                                '88624459 Object-Oriented Analysis and Design'),
                            subtitle: Text('Mr.Worawit Werapan'),
                          ),
                        ],
                      ),
                    ),
                    Card(
                      child: ExpansionTile(
                        title: Text('second semester'.tr.capitalizeFirst ??
                            'second semester'.tr),
                        controlAffinity: ListTileControlAffinity.trailing,
                        children: const [
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'F',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text('88624559	Software Testing'),
                            subtitle: Text('Dr.Pichet Wayalun'),
                          ),
                          ListTile(
                            minLeadingWidth: 10,
                            trailing: Text(
                              'F',
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            title: Text(
                                '88624459 Object-Oriented Analysis and Design'),
                            subtitle: Text('Mr.Worawit Werapan'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Row yearChoose() {
    return Row(
      children: [
        Expanded(
          child: InkWell(
            splashColor: Colors.transparent,
            onTap: () {
              tap63(true);
              tap64(false);
              tap65(false);
              pageController.animateToPage(
                0,
                duration: const Duration(milliseconds: 300),
                curve: Curves.fastLinearToSlowEaseIn,
              );
            },
            onHover: (value) {
              hover63(value);
            },
            child: Obx(
              () => AnimatedContainer(
                decoration: BoxDecoration(
                  color: hover63.isTrue || tap63.isTrue
                      ? Colors.amber[100]
                      : Colors.blueGrey,
                  borderRadius: const BorderRadius.horizontal(
                    left: Radius.circular(20),
                  ),
                ),
                height: 30,
                duration: const Duration(milliseconds: 300),
                child: Center(
                  child: Text(
                    "2563",
                    style: TextStyle(
                      color: hover63.isTrue || tap63.isTrue
                          ? Colors.black
                          : Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: InkWell(
            splashColor: Colors.transparent,
            onTap: () {
              tap63(false);
              tap64(true);
              tap65(false);
              pageController.animateToPage(
                1,
                duration: const Duration(milliseconds: 300),
                curve: Curves.fastLinearToSlowEaseIn,
              );
            },
            onHover: (value) {
              hover64(value);
            },
            child: Obx(
              () => AnimatedContainer(
                decoration: BoxDecoration(
                  color: hover64.isTrue || tap64.isTrue
                      ? Colors.amber[100]
                      : Colors.blueGrey,
                ),
                height: 30,
                duration: const Duration(milliseconds: 300),
                child: Center(
                  child: Text(
                    "2564",
                    style: TextStyle(
                      color: hover64.isTrue || tap64.isTrue
                          ? Colors.black
                          : Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: InkWell(
            splashColor: Colors.transparent,
            onTap: () {
              tap63(false);
              tap64(false);
              tap65(true);
              pageController.animateToPage(
                2,
                duration: const Duration(milliseconds: 300),
                curve: Curves.fastLinearToSlowEaseIn,
              );
            },
            onHover: (value) {
              hover65(value);
            },
            child: Obx(
              () => AnimatedContainer(
                decoration: BoxDecoration(
                  color: hover65.isTrue || tap65.isTrue
                      ? Colors.amber[100]
                      : Colors.blueGrey,
                  borderRadius: const BorderRadius.horizontal(
                    right: Radius.circular(20),
                  ),
                ),
                duration: const Duration(milliseconds: 300),
                height: 30,
                child: Center(
                  child: Text(
                    "2565",
                    style: TextStyle(
                      color: hover65.isTrue || tap65.isTrue
                          ? Colors.black
                          : Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  SizedBox summaryBox() {
    return SizedBox(
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 140,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.6),
              borderRadius: const BorderRadius.horizontal(
                left: Radius.circular(20),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: const [
                Text(
                  '3.45',
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  'GPA : 2-2565',
                  style: TextStyle(fontSize: 13),
                ),
              ],
            ),
          ),
          Container(
            width: 140,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.2),
              borderRadius: const BorderRadius.horizontal(
                right: Radius.circular(20),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text(
                  '3.12',
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  'summary'.tr.capitalize ?? 'summary'.tr,
                  style: const TextStyle(fontSize: 13),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
