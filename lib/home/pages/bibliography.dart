import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Bibligraphy extends StatefulWidget {
  const Bibligraphy({super.key});

  @override
  State<Bibligraphy> createState() => _BibligraphyState();
}

class _BibligraphyState extends State<Bibligraphy> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      children: [
        Card(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListTile(
                  leading: Image.asset('assets/images/Airbnb_Joe_Square.jpg'),
                  title: const Text("Peter Yamada"),
                  subtitle: Text('${'ID'.tr}: 1610212308'),
                  trailing: Text.rich(
                    TextSpan(text: '${'status'.tr.capitalize}: ', children: [
                      TextSpan(
                        text: '${'studying'.tr.capitalize}',
                        style: const TextStyle(
                          color: Colors.green,
                        ),
                      )
                    ]),
                  ),
                ),
                const Divider(),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '${'faculty'.tr.capitalize}: ${'informatics'.tr.capitalize}',
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '${'campus'.tr.capitalize}: ${'bangsaen'.tr.capitalize}',
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '${'degree'.tr.capitalize}: ${'undergraduate'.tr.capitalize}',
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '${'degree name'.tr.capitalize}: ${'B.Sc. in Computer Science '.tr.capitalize}',
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '${'advisor'.tr.capitalize}: ${'Asst Prof Dr. KOMATE AMPHAWAN, MR. PUSIT KULKASEM'.tr.capitalize}',
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Card(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${'personal data'.tr.capitalize}',
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const Divider(),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '${'nationality'.tr.capitalize}: ${'thai'.tr.capitalize}',
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '${'RELIGION'.tr.capitalize}: ${'Is-lam'.tr.capitalize}',
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '${'BLOOD GROUP'.tr.capitalize}: ${'b'.tr.capitalize}',
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '${'degree name'.tr.capitalize}: ${'B.Sc. in Computer Science '.tr.capitalize}',
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
