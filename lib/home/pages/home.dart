import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      children: [
        Card(
          color: Colors.transparent,
          shadowColor: Colors.transparent,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTile(
                  leading: ClipOval(
                    child: Image.asset("assets/images/Airbnb_Joe_Square.jpg"),
                  ),
                  title: Text('${'welcome'.tr.capitalizeFirst}, Peter'),
                  subtitle: const Text('1610212308'),
                  trailing: Text.rich(
                    TextSpan(text: '${'status'.tr.capitalize}: ', children: [
                      TextSpan(
                        text: '${'studying'.tr.capitalize}',
                        style: const TextStyle(
                          color: Colors.green,
                        ),
                      )
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
        const Divider(
          thickness: 1,
          indent: 40,
          endIndent: 40,
        ),
        onGoing(),
        post(
          title: "การทำบัตรนิสิตกับธนาคารกรุงไทย",
          subTitle: """กรณีบัตรหายเสียค่าใช้จ่ายในการทำ 100 บาท
สำหรับนิสิตรหัส 65 วิทยาเขตบางแสนที่เข้าภาคเรียนที่ 1 ที่ยังไม่รับบัตรให้ติดต่อรับบัตรนิสิตที่ธนาคารกรุงไทย สาขา ม.บูรพา ส่วนนิสิตที่เข้าภาคเรียนที่ 2/2565 รอกำหนดการอีกครั้ง""",
          child: Column(
            children: [
              Image.asset("assets/images/card1_1.jpg"),
              Image.asset("assets/images/card1_3.png")
            ],
          ),
        )
      ],
    );
  }

  Card onGoing() {
    return Card(
      shadowColor: Colors.green,
      color: Colors.green[50],
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: ListTile(
          leading: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const Icon(
                Icons.access_time,
                color: Colors.blueGrey,
              ),
              Text(
                'now'.tr.capitalize ?? 'now'.tr,
                style: const TextStyle(
                  fontSize: 8,
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
          title: const Text(
            '88634459 Mobile Application Development I',
            style: TextStyle(fontSize: 12),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${'lect.'.tr.capitalizeFirst}: Asst. Prof. Dr.Jakkarin Suksawatchon',
                style: const TextStyle(fontSize: 10),
              ),
              Text(
                '${'room'.tr.capitalizeFirst}: IF-4C01',
                style: const TextStyle(fontSize: 10),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget post({required String title, String? subTitle, Widget? child}) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
        child: Column(
          children: [
            ListTile(
              title: Text(
                title,
                style: const TextStyle(fontSize: 12),
              ),
              subtitle: subTitle == null
                  ? null
                  : Text(
                      subTitle,
                      style: const TextStyle(fontSize: 10),
                    ),
            ),
            const Divider(
              thickness: 0.5,
              indent: 20,
              endIndent: 20,
            ),
            child ?? Container(),
          ],
        ),
      ),
    );
  }
}
