import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TimeTable extends StatefulWidget {
  const TimeTable({super.key});

  @override
  State<TimeTable> createState() => _TimeTableState();
}

class _TimeTableState extends State<TimeTable> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      children: [
        Card(
          child: ExpansionTile(
            title: Text('monday'.tr.capitalizeFirst ?? 'monday'.tr),
            controlAffinity: ListTileControlAffinity.trailing,
            children: [
              ListTile(
                minLeadingWidth: 10,
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text(
                      '10.00 AM',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                    Text(
                      '12.00 AM',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                title: const Text('88624559	Software Testing'),
                subtitle: const Text('Dr.Pichet Wayalun'),
              ),
              ListTile(
                minLeadingWidth: 10,
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text(
                      '01.00 PM',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                    Text(
                      '03.00 PM',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                title:
                    const Text('88624459 Object-Oriented Analysis and Design'),
                subtitle: const Text('Mr.Worawit Werapan'),
              ),
            ],
          ),
        ),
        Card(
          child: ExpansionTile(
            title: Text('tuesday'.tr.capitalizeFirst ?? 'tuesday'.tr),
            controlAffinity: ListTileControlAffinity.trailing,
            children: [
              ListTile(
                minLeadingWidth: 10,
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text(
                      '10.00 AM',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                    Text(
                      '12.00 AM',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                title: const Text('88624559	Software Testing'),
                subtitle: const Text('Dr.Pichet Wayalun'),
              ),
              ListTile(
                minLeadingWidth: 10,
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text(
                      '01.00 PM',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                    Text(
                      '03.00 PM',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                title:
                    const Text('88624459 Object-Oriented Analysis and Design'),
                subtitle: const Text('Mr.Worawit Werapan'),
              ),
            ],
          ),
        ),
        Card(
          child: ExpansionTile(
            title: Text('wednesday'.tr.capitalizeFirst ?? 'wednesday'.tr),
            controlAffinity: ListTileControlAffinity.trailing,
            children: [
              ListTile(
                minLeadingWidth: 10,
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text(
                      '10.00 AM',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                    Text(
                      '12.00 AM',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                title: const Text('88624559	Software Testing'),
                subtitle: const Text('Dr.Pichet Wayalun'),
              ),
              ListTile(
                minLeadingWidth: 10,
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text(
                      '01.00 PM',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                    Text(
                      '03.00 PM',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                title:
                    const Text('88624459 Object-Oriented Analysis and Design'),
                subtitle: const Text('Mr.Worawit Werapan'),
              ),
            ],
          ),
        ),
        Card(
          child: ExpansionTile(
            title: Text('thursday'.tr.capitalizeFirst ?? 'thursday'.tr),
            controlAffinity: ListTileControlAffinity.trailing,
            children: [
              ListTile(
                minLeadingWidth: 10,
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text(
                      '10.00 AM',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                    Text(
                      '12.00 AM',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                title: const Text('88624559	Software Testing'),
                subtitle: const Text('Dr.Pichet Wayalun'),
              ),
              ListTile(
                minLeadingWidth: 10,
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text(
                      '01.00 PM',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                    Text(
                      '03.00 PM',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                title:
                    const Text('88624459 Object-Oriented Analysis and Design'),
                subtitle: const Text('Mr.Worawit Werapan'),
              ),
            ],
          ),
        ),
        Card(
          child: ExpansionTile(
            title: Text('friday'.tr.capitalizeFirst ?? 'friday'.tr),
            controlAffinity: ListTileControlAffinity.trailing,
            children: [
              ListTile(
                minLeadingWidth: 10,
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text(
                      '10.00 AM',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                    Text(
                      '12.00 AM',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                title: const Text('88624559	Software Testing'),
                subtitle: const Text('Dr.Pichet Wayalun'),
              ),
              ListTile(
                minLeadingWidth: 10,
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Text(
                      '01.00 PM',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                    Text(
                      '03.00 PM',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                title:
                    const Text('88624459 Object-Oriented Analysis and Design'),
                subtitle: const Text('Mr.Worawit Werapan'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
