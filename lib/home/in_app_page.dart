import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:register/controller/page.dart';
import 'package:register/home/login.dart';
import 'package:register/home/pages/bibliography.dart';
import 'package:register/home/pages/home.dart';
import 'package:register/home/pages/result.dart';
import 'package:register/home/pages/time_table.dart';
import 'package:register/session.dart';
import 'package:responsive_builder/responsive_builder.dart';

class InAppPage extends StatefulWidget {
  const InAppPage({super.key});

  @override
  State<InAppPage> createState() => _InAppPageState();
}

class _InAppPageState extends State<InAppPage> {
  final InAppPageController _pageController = Get.put(InAppPageController());

  List<String> titles = [
    'announce'.tr,
    'time table'.tr,
    'result'.tr,
    'bibliography'.tr,
  ];

  @override
  Widget build(BuildContext context) {
    var appbar = AppBar(
      elevation: 0.1,
      backgroundColor: Colors.white,
      foregroundColor: const Color.fromARGB(255, 189, 150, 32),
      title: Obx(
        () => Text(
          titles[_pageController.activePage.toInt()].capitalizeFirst ??
              titles[_pageController.activePage.toInt()],
        ),
      ),
      actions: [
        IconButton(
          onPressed: () {
            Get.updateLocale(
              Get.locale == const Locale("en")
                  ? const Locale("th")
                  : const Locale("en"),
            );
          },
          icon: const Icon(Icons.language_outlined),
        )
      ],
    );
    return Scaffold(
      appBar: appbar,
      drawer: Drawer(
        elevation: 0.2,
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blueGrey.withOpacity(0.1),
              ),
              child: Image.asset(
                'assets/images/logo_buu-03-transformed.png',
                height: 80,
              ),
            ),
            ListTile(
              leading: const Icon(Icons.newspaper_sharp),
              title: Text('announce'.tr.capitalizeFirst ?? 'announce'.tr),
              onTap: () {
                _pageController.activePage(0);
              },
            ),
            ListTile(
              leading: const Icon(Icons.access_time_sharp),
              title: Text('time table'.tr.capitalizeFirst ?? 'time table'.tr),
              onTap: () {
                _pageController.activePage(1);
              },
            ),
            ListTile(
              leading: const Icon(Icons.description_outlined),
              title: Text('result'.tr.capitalizeFirst ?? 'result'.tr),
              onTap: () {
                _pageController.activePage(2);
              },
            ),
            ListTile(
              leading: const Icon(Icons.person),
              title:
                  Text('bibliography'.tr.capitalizeFirst ?? 'bibliography'.tr),
              onTap: () {
                _pageController.activePage(3);
              },
            ),
            const Divider(
              thickness: 1,
              indent: 10,
              endIndent: 10,
            ),
            ListTile(
              leading: const Icon(Icons.logout),
              title: Text('logout'.tr.capitalizeFirst ?? 'logout'.tr),
              onTap: () {
                Session.setLogin(false);
                Get.offAll(() => const LoginPage());
              },
            )
          ],
        ),
      ),
      body: Obx(
        () {
          return IndexedStack(
            index: _pageController.activePage.toInt(),
            children: const [Home(), TimeTable(), Result(), Bibligraphy()],
          );
        },
      ),
    );
  }
}
