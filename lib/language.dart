import 'package:get/get.dart';

class Languages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'th_TH': {
          'announce': 'ประกาศ',
          'time table': 'ตารางเรียน',
          'result': 'ผลการศึกษา',
          'bibliography': 'ประวัตินิสิต',
          'logout': 'ออกจากระบบ',
          'status': 'สถานะ',
          'studying': 'กำลังศึกษา',
          'alert': 'คำเตือน',
          'username': 'บัญชีผู้ใช้',
          'password': 'รหัสผ่าน',
          'log in': 'เข้าสู่ระบบ',
          'ok': 'รับทราบ',
          'welcome': 'ยินดีต้อนรับ',
          'monday': 'วันจันทร์',
          'tuesday': 'วันอังคาร',
          'wednesday': 'วันพุธ',
          'thursday': 'วันพฤหัสบดี',
          'friday': 'วันศุกร์',
          'saturday': 'วันเสาร์',
          'sunday': 'วันอาทิตย์',
        },
      };
}
