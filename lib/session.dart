import 'package:shared_preferences/shared_preferences.dart';

class Session {
  static Future<void> setLogin(bool login) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool('login', login);
  }

  static Future<bool?> getLogin() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool('login');
  }
}
