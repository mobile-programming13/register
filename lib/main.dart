import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:register/home/in_app_page.dart';
import 'package:register/home/login.dart';
import 'package:register/language.dart';
import 'package:register/session.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      translations: Languages(),
      locale: Get.deviceLocale,
      debugShowCheckedModeBanner: false,
      title: 'Profile',
      fallbackLocale: const Locale('en', 'US'),
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: FutureBuilder(
        future: Session.getLogin(),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.data ?? false) {
            return const InAppPage();
          } else {
            return const LoginPage();
          }
        },
      ),
    );
  }
}
